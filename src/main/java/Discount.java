/**
 * Created by liukaixin on 16/5/19.
 */
public class Discount {
    private String type;
    private String[] discountItems;

    public void setType(String type) {
        this.type = type;
    }

    public void setDiscountItems(String[] discountItems) {
        this.discountItems = discountItems;
    }

    public String getType() {
        return type;
    }

    public String[] getDiscountItems() {
        return discountItems;
    }
}
