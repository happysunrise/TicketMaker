import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by liukaixin on 16/5/19.
 */
public class Client {

    public static void main(String[] args) throws IOException {

        StringBuilder goodsListFromFile = new StringBuilder();
        Files.lines(Paths.get("goods_list.txt"), StandardCharsets.UTF_8).forEach(goodsListFromFile::append);
        TicketMaker ticketMaker = new TicketMaker();
        ticketMaker.setGoods(goodsListFromFile.toString());

        StringBuilder discountGoodsFromFile = new StringBuilder();
        Files.lines(Paths.get("discount_goods_list.txt"), StandardCharsets.UTF_8).forEach(discountGoodsFromFile::append);
        DiscountManager.getDiscountManager().setDiscountGoods(discountGoodsFromFile.toString());

        StringBuilder productListFromFile = new StringBuilder();
        Files.lines(Paths.get("product_list.txt"), StandardCharsets.UTF_8).forEach(productListFromFile::append);
        ProductManager.getProductManager().setProduct(productListFromFile.toString());

        ticketMaker.printTicket();
    }

}
