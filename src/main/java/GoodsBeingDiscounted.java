/**
 * Created by liukaixin on 16/5/19.
 */
public class GoodsBeingDiscounted {

    private String discountType;
    private String name;
    private Integer originCount;//优惠前的购买数量
    private Integer actualCount;//优惠后折合成的购买数量
    private double priceAfterDiscount; //优惠后的总价
    private double priceBeforeDiscount;//优惠前的总价

    public GoodsBeingDiscounted(String discountType, String name, Integer originCount, Integer actualCount, double priceAfterDiscount, double originalPrice) {
        this.discountType = discountType;
        this.name = name;
        this.originCount = originCount;
        this.actualCount = actualCount;
        this.priceAfterDiscount = priceAfterDiscount;
        this.priceBeforeDiscount = originalPrice;
    }

    public String getName() {
        return name;
    }

    public Integer getOriginCount() {
        return originCount;
    }

    public Integer getActualCount() {
        return actualCount;
    }

    public double getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public double getPriceBeforeDiscount() {
        return priceBeforeDiscount;
    }

}
