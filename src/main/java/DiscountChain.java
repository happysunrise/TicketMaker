
/**
 * Created by liukaixin on 16/5/19.
 */
public interface DiscountChain {
    void next(DiscountChain nextChain);
    GoodsBeingDiscounted getPriceAfterDiscount(Product product, int count);
}
