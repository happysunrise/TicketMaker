/**
 * Created by liukaixin on 16/5/19.
 */
public class BuyThreeFreeOneDiscountHandler implements DiscountChain {

//    private DiscountChain nextChain;

    @Override
    public void next(DiscountChain nextChain) {
//        this.nextChain = nextChain;
    }

    @Override
    public GoodsBeingDiscounted getPriceAfterDiscount(Product product, int originalCount) {
        if (DiscountManager.getDiscountManager().getDiscountType(product.getBarcode()).equals(DiscountManager.BUY_THREE_GET_ONE_FREE)) {
            return new GoodsBeingDiscounted(DiscountManager.BUY_THREE_GET_ONE_FREE, product.getName(), originalCount, getCountAfterDiscountFor(originalCount), product.getPrice() * getCountAfterDiscountFor(originalCount), product.getPrice() * originalCount);
        }
        return new GoodsBeingDiscounted(DiscountManager.NO_DISCOUNT, product.getName(), originalCount, originalCount, product.getPrice() * originalCount, product.getPrice() * originalCount);
    }

    //获得优惠后的商品数量
    private int getCountAfterDiscountFor(int originalCount) {
        return originalCount - originalCount / 3;
    }

}
