/**
 * Created by liukaixin on 16/5/19.
 */
public class Product {
    private double price;
    private String barcode;
    private String name;
    private String unit;
    private String category;
    private String subCategory;

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getCategory() {
        return category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public double getPrice() {
        return price;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
