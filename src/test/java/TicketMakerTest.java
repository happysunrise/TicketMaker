import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by liukaixin on 16/5/19.
 */
public class TicketMakerTest {

    private ProductManager productManager = ProductManager.getProductManager();
    private DiscountManager discountManager = DiscountManager.getDiscountManager();

    @Before
    public void setUp() throws Exception {
        productManager.setProduct("[" +
                "{" +
                "  barcode: 'ITEM000003'," +
                "  name: '苹果'," +
                "  unit: '斤'," +
                "  category: '食品'," +
                "  subCategory: '水果'," +
                "  price: 5.50" +
                "}" +
                "," +
                "{" +
                "  barcode: 'ITEM000001'," +
                "  name: '羽毛球'," +
                "  unit: '个'," +
                "  category: '运动'," +
                "  subCategory: '球类'," +
                "  price: 1.00" +
                "}" +
                "," +
                "{" +
                "  barcode: 'ITEM000005'," +
                "  name: '可口可乐'," +
                "  unit: '瓶'," +
                "  category: '食品'," +
                "  subCategory: '碳酸饮料'," +
                "  price: 3.00" +
                "}" +
                "]");
        discountManager.setDiscountGoods("[" +
                "  {" +
                "    type: 'BUY_THREE_GET_ONE_FREE'," +
                "    barcodes: [" +
                "      'ITEM000001'," +
                "      'ITEM000005'" +
                "    ]" +
                "  }" +
                "]");
    }

    //统计购物车中某商品的数量,json中不带-
    @Test
    public void should_return_1_if_buy_one_item() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000001'," +
                "]");

        //then
        assertEquals("failure - the count of items wrong", 1, ticketMaker.getCountOf("ITEM000001"), 0);
    }

    @Test
    public void should_return_4_if_buy_4_item1() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000001'," +
                "    'ITEM000001'," +
                "    'ITEM000002'," +
                "    'ITEM000001'," +
                "    'ITEM000002'," +
                "    'ITEM000001'," +
                "]");

        //then
        assertEquals("failure - the count of items wrong", 4, ticketMaker.getCountOf("ITEM000001"), 0);
    }

    //购物车中商品条码带"-"时,统计该商品数量
    @Test
    public void should_return_2_if_buy_2_item1_with_json_including_line() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000001-2'," +
                "]");

        //then
        assertEquals("failure - the count of items with - wrong", 2, ticketMaker.getCountOf("ITEM000001"), 0);
    }

    @Test
    public void should_return_5_if_buy_5_item1_with_json_including_line() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000001-2'," +
                "    'ITEM000001'," +
                "    'ITEM000002'," +
                "    'ITEM000001'," +
                "    'ITEM000002'," +
                "    'ITEM000001'," +
                "]");

        //then
        assertEquals("failure - the count of items with - wrong", 5, ticketMaker.getCountOf("ITEM000001"), 0);
    }

    //获得某商品的优惠类型
    public void should_return_null_if_not_in_discount_goods() {
        //then
        assertEquals("failure - the discount type of goods is wrong", "NO DISCOUNT", discountManager.getDiscountType("ITEM000005"));
    }

    @Test
    public void should_return_discount_type_if_is_discount_good() {
        //then
        assertEquals("failure - the discount type of goods is wrong", "BUY_THREE_GET_ONE_FREE", discountManager.getDiscountType("ITEM000001"));
    }

    //获得某商品的属性
    @Test
    public void get_product_properties_from_json() {
        Product product = productManager.getProductByBarcode("ITEM000003");
        //then
        assertEquals("failure - get product properties from json is wrong", "ITEM000003,苹果,斤,食品,水果", product.getBarcode() + "," + product.getName() + "," + product.getUnit() + "," + product.getCategory() + "," + product.getSubCategory());
        assertEquals("failure - get product properties from json is wrong", 5.50, product.getPrice(), 0.00);
    }

    //获得某商品的小计价格
    @Test
    public void should_return_11_if_buy_2_apple_when_apple_is_not_discount_good() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000003-2'," +
                "]");
        //then
        assertEquals("failure - the total price for good which isn't discount good is wrong", 11.00, ticketMaker.getTotalPriceFor("ITEM000003"), 0.00);
    }

    //计算某件商品买三免一优惠后的价格
    @Test
    public void should_return_6_if_buy_3_cola_when_cola_is_discount_good() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
//        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000005'," +
                "    'ITEM000005-2'," +
                "]");
        //then
        assertEquals("failure - the total price for good which is discount good is wrong", 6.00, ticketMaker.getTotalPriceFor("ITEM000005"), 0.00);
    }

    //计算商品总价
    @Test
    public void print_ticket_when_only_buy_one_item() {
        //given
        TicketMaker ticketMaker = new TicketMaker();
        //when
        ticketMaker.setGoods("[" +
                "    'ITEM000001'," +
                "    'ITEM000001'," +
                "    'ITEM000001'," +
                "    'ITEM000001'," +
                "    'ITEM000001'," +
                "    'ITEM000003-2'," +
                "    'ITEM000005'," +
                "    'ITEM000005'," +
                "    'ITEM000005'" +
                "]");

        //then
        assertEquals("failure - ticket print wrong", 21.00, ticketMaker.getTotalPrice(), 0.00);
    }
}
